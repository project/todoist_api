<?php

/**
 * @file
 * Administrative page callbacks for the todoist api module.
 */

/**
 * Implements hook_form().
 */
function todoist_api_config_form($form, &$form_state) {
  $form = array();

  $form['todoist_api_user_token'] = array(
    '#type' => 'textfield',
    '#title' => 'ToDoist Api Token',
    '#description' => t('Get your ToDoist API token from !todoist.', array(
      "!todoist" => l(t('todoist'), "https://todoist.com/"),
    )),
    '#default_value' => variable_get('todoist_api_user_token'),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'todoist_api_config_form_validate';
  return system_settings_form($form);
}

/**
 * Implements Validation handler for todoist_config_form().
 */
function todoist_api_config_form_validate($form, &$form_state) {
  if ($form_state['values']['todoist_api_user_token']) {
    $apicall = TODOIST_API;
    $apicall .= 'token=' . $form_state['values']['todoist_api_user_token'];
    $http_result = drupal_http_request($apicall);
    if ($http_result->status_message !== 'OK') {
      form_set_error('todoist_api_user_token', t('You have entered wrong token value please check.'));
    }
  }
}
