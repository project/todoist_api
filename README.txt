Introduction
============

This module intregate todoist api functionality in drupal project. Using this
module user can access their todoist.com account basic functionality in drupal
project. They can add, edit, delete and complete their task. They will get
remender mail time to time sent by todoist.com.

Requirements
============

- Yes: user have to create account on todoist.com and get user token from there.

Version
========

- ToDoist API version V8.


Features
========

- User can access their todoist.com account in drupal instance.
- They can delete, add, edit and complete their task from drupal instance.
- Everything they will do in drupal instance (under todoist) will automatically
  updated on todoist.com.
- User can create unlimited number of task.
- Its an independent module, no need of any dependencies.

INSTALLATION
============

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

* On Todoist website

- First you have to signup or login on Todoist.com.
- After sign-up/login on Todoist official website, please go to user account
  section and here you have to create an api key.

* On your Drupal website

- Download this module and enable it.
- Copy the API key (which you have created on Todoist official website) and
  paste it to your Drupal website under Todoist configuration setting.
- Now you get all your task, which you created pervious on Todoist official
  website or you can able to edit/delete task here. You can also add new
  task here. Every time when you create new task or edited any task,
  this will save on your Todoist.com account.

CONFIGURATION
=============

* Configure user permissions in Administration » People » Permissions:
* Config todoist in Administration » User interface » Todoist configuration:

MAINTAINERS
===========

Current maintainers:
 * Arjun Kumar (Manav) - https://www.drupal.org/u/manav
