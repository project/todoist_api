<?php

/**
 * @file
 * Generate function for task list, add task, edit, delete and complete.
 */

/**
 * Implements todoist_api_content_list().
 *
 * Returns list of content.
 */
function todoist_api_content_list() {
  $token = variable_get('todoist_api_user_token');
  global $base_url;
  if (!$token) {
    drupal_set_message(t('You have to add your Todoist details here then you can access the list of all your task'), 'error');
    drupal_goto('admin/config/user-interface/todoist');
  }
  else {
    // Get api token.
    $apicall = TODOIST_API;
    $apicall .= 'token=' . $token;
    $apicall .= '&sync_token=*&resource_types=["all"]';
    $http_result = drupal_http_request($apicall);
    if ($http_result->status_message !== 'OK') {
      return drupal_set_message(t('There is an error in your configuration settings please check your ToDoist Api token setting under @url', array('@url' => l(t('Click here'), $base_url . '/admin/config/user-interface/todoist')), 'warning'));
    }
    else {
      $data = $http_result->data;
      $data = drupal_json_decode($data);
      $items = isset($data['items']) ? $data['items'] : '';
      $header = array(
        array('data' => t('Task'), 'sort' => 'asc'),
        array('data' => t('Task added date')),
        array('data' => t('Task due date')),
        array('data' => t('Action'), 'colspan' => 3),
      );
      $rows = array();
      if ($items) {
        foreach ($items as $node) {
          $rows[] = array(
            $node['content'],
            isset($node['date_added']) ? date('D, j M Y', strtotime($node['date_added'])) : t('No date'),
            !empty($node['date_string']) ? $node['date_string'] : 'No due date',
            l(t('Complete'), 'todoist/' . $node['id'] . '/complete'),
            l(t('Edit'), 'todoist/' . $node['id'] . '/edit/'),
            l(t('Delete'), 'todoist/' . $node['id'] . '/delete'),
          );
        }
      }

      $html = theme('table',
        array(
          'header' => $header,
          'rows' => $rows,
          // Optional to indicate whether the table headers should be sticky.
          'sticky' => TRUE,
          // Optional empty text for the table if resultset is empty.
          'empty' => t('No task created...'),
        )
      );

      return $html;
    }
  }
}

/**
 * Implements hook_form().
 *
 * Here we are set up task add form for todist api module.
 */
function todoist_api_task_add_form($form, $form_state) {

  $form = array();

  $form['task_content'] = array(
    '#title' => 'Task',
    '#type' => 'textfield',
    '#description' => t('Add task heading'),
    '#required' => TRUE,
  );

  $form['task_due_data'] = array(
    '#title' => 'Due data',
    '#type' => 'textfield',
    '#description' => t('Add due data in mm/dd/yy format e.g fri at 2pm, today,
      today at 2, 10/5, 10/5/2006 at 2pm, 5 10 2010 etc. for more info please
      check this link !todoist_datatime', array("!todoist_datatime" => l(t('Todoist datatime'), "https://todoist.com/Help/DatesTimes"))),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['cancel'] = array(
    '#title' => t('Cancel'),
    '#type' => 'link',
    '#href' => 'admin/content/todoist',
  );

  return $form;
}

/**
 * Implements todoist_api_task_add_form_submit().
 *
 * Submit handler for todoist_api_task_add_form().
 */
function todoist_api_task_add_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {

    case 'Save':
      if ($form_state['values']['task_content'] && $form_state['values']['task_due_data']) {

        $content = $form_state['values']['task_content'];
        $date = $form_state['values']['task_due_data'];

        $apicall = 'https://todoist.com/API/v8/items/add?';

        $data = array(
          // Get api token.
          'token' => variable_get('todoist_api_user_token'),
          'content' => $content,
          'date_string' => $date,
        );

        $api = url($apicall, array('query' => $data));
        $http_result = drupal_http_request($api);

        if ($http_result->status_message == 'OK') {
          drupal_set_message(t('Task successfully added.'), 'status');
          $form_state['redirect'] = 'admin/content/todoist';
        }
        else {
          drupal_set_message(t('Please check your data or configuration setting.'), 'warning');
        }
      }
      break;
  }
}

/**
 * Implements hook_form().
 *
 * Here we are set up task delete form for todist api module.
 */
function todoist_api_task_delete_form($form, &$form_state, $task_id) {

  $form = array();

  $form['task_id'] = array(
    '#type' => 'value',
    '#value' => $task_id,
  );

  // Confirmation message.
  return confirm_form(
    $form,
    t('Are you sure you want to delete this task?'),
    'admin/content/todoist/',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );

}

/**
 * Implements todoist_api_task_delete_form_submit().
 *
 * Here we are set up form for todoist task delete.
 */
function todoist_api_task_delete_form_submit($form, &$form_state) {

  if ($form_state['values']['task_id']) {
    $ids = $form_state['values']['task_id'];
    $token = variable_get('todoist_api_user_token');
    $apicall = TODOIST_API;
    $post_data = array(
      'token' => $token,
      'commands' => '[{"type": "item_delete", "uuid": "f8539c77-7fd7-4846-afad-3b201f0be8a5", "args": {"ids": [' . $ids . ']}}]',
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apicall);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_exec($ch);
    if (curl_errno($ch)) {
      drupal_set_message(t('Error: @error', array("@error" => curl_error($ch))), 'error');
    }
    else {
      curl_close($ch);
      drupal_set_message(t('1 task deleted successfully.'));
      $form_state['redirect'] = 'admin/content/todoist/';
    }
  }
}

/**
 * Implements hook_form().
 *
 * Here we are set up form for task complete.
 */
function todoist_api_task_complete_form($form, &$form_state, $task_id) {

  $form = array();

  $form['task_id'] = array(
    '#type' => 'value',
    '#value' => $task_id,
  );

  // Confirm message.
  return confirm_form(
    $form,
    t('Nice, you have completed your task.'),
    'admin/content/todoist/',
    t('This action cannot be undone.'),
    t('Complete'),
    t('Cancel')
  );

}

/**
 * Implements todoist_api_task_complete_form_submit()
 *
 * Submit handler for todoist_api_task_complete_form().
 */
function todoist_api_task_complete_form_submit($form, &$form_state) {

  if ($form_state['values']['task_id']) {
    $ids = $form_state['values']['task_id'];
    $token = variable_get('todoist_api_user_token');
    $apicall = TODOIST_API;
    $post_data = array(
      'token' => $token,
      'commands' => '[{"type": "item_complete", "uuid": "a74bfb5c-' . todoist_api_gerahash(4) . '-4d14-baea-' . todoist_api_gerahash(12) . '", "args": {"ids": [' . $ids . ']}}]',
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apicall);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_exec($ch);
    if (curl_errno($ch)) {
      drupal_set_message(t('Error: @error', array("@error" => curl_error($ch))), 'error');
    }
    else {
      curl_close($ch);
      drupal_set_message(t('1 task completed successfully.'));
      $form_state['redirect'] = 'admin/content/todoist/';
    }
  }
}

/**
 * Implements hook_form().
 *
 * Here we are set up form for Todoist task edit.
 */
function todoist_api_task_edit_form($form, $form_state, $task_id) {

  $ids = $task_id;
  $apicall = 'https://todoist.com/API/v8/items/get?';
  $apicall .= '&token=' . variable_get('todoist_api_user_token');
  $apicall .= '&item_id=' . $ids;

  $http_result = drupal_http_request($apicall);
  if ($http_result->status_message == 'OK') {
    $datas = drupal_json_decode($http_result->data);
    $data = $datas['item'];
  }

  $form = array();

  $form['task_id'] = array(
    '#type' => 'value',
    '#value' => $task_id,
  );

  $form['task_content'] = array(
    '#title' => 'Task',
    '#type' => 'textfield',
    '#default_value' => $data['content'],
    '#description' => t('Add task heading'),
    '#required' => TRUE,
  );

  $form['task_due_data'] = array(
    '#title' => 'Due data',
    '#type' => 'textfield',
    '#default_value' => $data['date_string'],
    '#description' => t('Add due data in mm/dd/yy format e.g fri at 2pm, today,
      today at 2, 10/5, 10/5/2006 at 2pm, 5 10 2010 etc. for more info please
      check this link !todoist_datatime', array("!todoist_datatime" => l(t('Todoist datatime'), "https://todoist.com/Help/DatesTimes"))),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['cancel'] = array(
    '#title' => t('Cancel'),
    '#type' => 'link',
    '#href' => 'admin/content/todoist',
  );

  return $form;
}

/**
 * Implements todoist_api_task_edit_form_submit().
 *
 * Submit handler for todoist_api_task_edit_form().
 */
function todoist_api_task_edit_form_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {

    case 'Save':

      if ($form_state['values']['task_content'] && $form_state['values']['task_due_data']) {

        $content = $form_state['values']['task_content'];
        $date = $form_state['values']['task_due_data'];
        $id = $form_state['values']['task_id'];
        $token = variable_get('todoist_api_user_token');
        $apicall = TODOIST_API;
        $post_data = array(
          'token' => $token,
          'commands' => '[{"type": "item_update", "uuid": "318d16a7-0c88-46e0-9eb5-cde6c72477c8", "args": {"id": ' . $id . ', "content": "' . $content . '", "date_string": "' . $date . '"}}]',
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apicall);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_exec($ch);
        if (curl_errno($ch)) {
          drupal_set_message(t('Error: @error', array("@error" => curl_error($ch))), 'error');
        }
        else {
          curl_close($ch);
          drupal_set_message(t('1 task updated successfully.'));
          $form_state['redirect'] = 'admin/content/todoist/';
        }
      }
      break;
  }
}

/**
 * Implements todoist_api_gerahash().
 *
 * Create random string for UUID edit task.
 */
function todoist_api_gerahash($qtd) {
  // Under the string $caracteres you write all the characters you want
  // to be used to randomly generate the code.
  $caracteres = 'abcdefghijklmnopqrstuvwxyz0123456789';
  $quantidadecaracteres = strlen($caracteres);
  $quantidadecaracteres--;

  $hash = NULL;
  for ($x = 1; $x <= $qtd; $x++) {
    $posicao = rand(0, $quantidadecaracteres);
    $hash .= substr($caracteres, $posicao, 1);
  }

  return $hash;
}
